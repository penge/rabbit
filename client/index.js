require('dotenv').config();

const amqp = require('amqplib');
const url = process.env.AMQP_URL;
const { queue } = require('../config');
const message = require('../utils/arg').read(2, 'message');

let channel;
amqp
  .connect(url)
  .then(conn => conn.createChannel())
  .then(ch => channel = ch)
  .then(() => channel.assertQueue(queue))
  .then(() => channel.sendToQueue(queue, new Buffer(message)))
  .then(() => {
    setTimeout(() => {
      console.log(`[x] Sent ${message}`);
      process.exit(0);
    }, 500)
  })
  .catch(err => console.log(err));
