require('dotenv').config();

const amqp = require('amqplib');
const url = process.env.AMQP_URL;
const { queue } = require('../config');
const mailer = require('../utils/mailer');

let channel;
amqp
  .connect(url)
  .then(conn => conn.createChannel())
  .then(ch => channel = ch)
  .then(() => channel.assertQueue(queue))
  .then(() => {
    console.log('[*] Waiting for messages in "%s". To exit press CTRL+C', queue);
    return channel.consume(queue, msg => {
      const message = msg.content.toString();
      console.log(`[x] Received ${message}`);
      mailer.send(message, () => {
        console.log(`[x] Delivered ${message}`);
        channel.ack(msg);
      })
    })
  })
  .catch(err => console.log(err));
