const options = require('./options');
const transporter = require('./transporter');

const send = (message, callback) => {
  let mailOptions = options(message);
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) { return console.log(error); }
    console.log('Sent ', info);
    callback(info);
  })
}

module.exports = { send };
