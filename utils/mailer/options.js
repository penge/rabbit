module.exports = (message) => ({
  from:    process.env.MAILER_FROM,
  to:      process.env.MAILER_TO,
  replyTo: process.env.MAILER_REPLYTO,
  subject: 'Hello!',
  text: message
})
