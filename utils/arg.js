const read = (index, key) => {
  let arg = process.argv[index];
  return arg || error(key);
}

const error = key => {
  console.log(`"${key}" argument is required!`);
  process.exit(1);
}

module.exports = { read };
