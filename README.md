# Setup

Make sure to have RabbitMQ installed and running.

# Usage

```
$ node server
$ node client "Hello, how are you?"
```

# ENV

In this simple case, .env is not excluded from repo.
